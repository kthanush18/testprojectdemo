package com.BitBucketTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class GoogleTest {
	
	@Test(testName = "google test")
	public void OpenBrowserAndNavigateToGoogle() {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com");
	}

}
